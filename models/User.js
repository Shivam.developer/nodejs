const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    firstName : { type: String },
    lastName : { type: String }
});

module.exports = mongoose.model('User', UserSchema);