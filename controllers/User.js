const User = require('../models/User');

const saveUser = (req, res, next) => 
{
    const user = new User({
        firstName : req.body.firstName,
        lastName : req.body.lastName
    });
    user.save().then((us) =>
    {
        res.status(200).json({
            message : "Entered Successfully",
            user : us
        });
    })
    .catch((err) => 
    {
        res.status(200).json({
            message : "Entry failed",
            errors : err
        });
    });
};

const fetchUser = async (req, res, next) =>
{
    try
    {
        const users = await User.find();
        res.status(200).json({
            users : users,
            msg : 1
        });
    }
    catch(err)
    {
        res.status(400).json({
            message : "Error",
            errors : err
        });
    }
};

module.exports = { saveUser, fetchUser };